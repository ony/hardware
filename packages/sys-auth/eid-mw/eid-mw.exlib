# Copyright 2009-2018 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases pkg_postinst pkg_postrm

require github [ user='Fedict' ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require gtk-icon-cache gsettings

SUMMARY="Belgian eID middleware"
DESCRIPTION="The eID Middleware software offers components for using the Belgian eID on your computer."
HOMEPAGE+=" http://eid.belgium.be"

LICENCES="LGPL-3"
SLOT="0"

MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        !app-misc/eid-mw
        dev-libs/glib:2[>=2.30]
        dev-libs/libxml2:2.0
        dev-libs/p11-kit:1
        net-libs/libproxy:1
        net-misc/curl
        sys-apps/pcsc-lite[>=1.4.4]
        x11-libs/gtk+:3
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    test:
        providers:ijg-jpeg? ( media-libs/jpeg:* )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    suggestion:
        app-crypt/acsccid[>=1.0.4] [[
            description = [ Use CCID drivers for ACS smartcard readers ]
        ]]
        app-crypt/ccid [[ description = [ Use CCID drivers for a bunch a smartcard readers ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-dialogs' '--with-gtkvers=3' )

eid-mw_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

eid-mw_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

