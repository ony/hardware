# Copyright 2016-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=kozec tag=v${PV} ] \
    setup-py [ import=setuptools has_bin=true multibuild=false blacklist=3 test=pytest ] \
    udev-rules [ udev_files=[ scripts/90-sc-controller.rules ] ] \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache

SUMMARY="User-mode driver, mapper and GTK3 based GUI for Steam Controller, DS4 and similar"

LICENCES="
    CC0 [[ note = [ images under the images/ directory ] ]]
    BSD-3 [[ note = [ lib/enum.py ] ]]
    GPL-2
    LGPL-2.1 [[ note = [ lib/usb1.py and lib/libusb1.py ] ]]
    PSF-2.2 [[ note = [ lib/jsonencoder.py ] ]]
    ZLIB [[ note = [ scripts/gamecontrollerdb.txt ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-python/evdev[python_abis:*(-)?]
        dev-python/pycairo[python_abis:*(-)?]
        dev-python/pylibacl[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
    run:
        dev-libs/libusb:1
        gnome-desktop/adwaita-icon-theme
        gnome-desktop/librsvg:2[gobject-introspection]
        x11-libs/gtk+:3[>=3.22]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

src_prepare() {
    # remove broken test, last checked: 0.4.4
    edo rm tests/test_setup.py

    default
}

src_install() {
    setup-py_src_install

    install_udev_files

    edo rm -rf "${IMAGE}"/usr/lib

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

