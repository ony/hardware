# Copyright 2008,2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2010 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules openrc-service

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="Official Linux Bluetooth protocol stack"
DESCRIPTION="
BlueZ provides support for the core Bluetooth layers and protocols. It is flexible,
efficient and uses a modular implementation. It has many interesting features:
* Complete modular implementation
* Symmetric multi processing safe
* Multithreaded data processing
* Support for multiple Bluetooth devices
* Real hardware abstraction
* Standard socket interface to all layers
* Device and service level security support
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="mirror://kernel/linux/bluetooth/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    alsa
    cups [[
        description = [ Include the CUPS driver for Bluetooth printers ]
        note = [ This option has not been tested ]
    ]]
    systemd
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

if ever at_least 5.50 ; then
    MYOPTIONS+="
        deprecated [[ description = [ Build deprecated tools (hciattach, hcitool, ...) ] ]]
        mesh [[ description = [ Bluetooth Low Energy (LE) Mesh profile support ] ]]
        nfc [[ description = [ Enable NFC pairing ] ]]
        obex [[ description = [ Include the OBEX profile (calendaring) ] ]]
    "
else
    MYOPTIONS+="
        gstreamer
        health [[
            description = [ Include HDP ( Health Device Profile ) support ]
            note = [ This option is uncommon and untested, so it is optional ]
        ]]
        legacy [[ description = [ Install the deprecated dund, hidd, and pand daemons ] ]]
        pcmcia [[ description = [ Install udev rules and scripts for PCMCIA devices   ] ]]
        usb [[ description = [ Enable support for USB devices including the tools hid2hci (needed to use some dell, logitech, and csr devices) and dfutool (used to update the device firmware) ] ]]
    "
fi

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.28]
        sys-apps/dbus[>=1.4]
        alsa? ( sys-sound/alsa-lib )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        cups? ( net-print/cups )
"

if ever at_least 5.50 ; then
    DEPENDENCIES+="
        build+run:
            sys-apps/dbus[>=1.6]
            sys-libs/readline:=
            mesh? (
                dev-libs/ell[>=0.3]
                dev-libs/json-c
            )
            obex? ( office-libs/libical:= )
        run:
            nfc? ( net/neard )
    "
else
    DEPENDENCIES+="
        build:
            (
                alsa? ( media-libs/libsndfile )
                gstreamer? ( media-libs/libsndfile )
            ) [[ note = [ automagic and used to build a single sbc test ] ]]
        build+run:
            gstreamer? (
                media-libs/gstreamer:0.10
                media-plugins/gst-plugins-base:0.10
            )
            usb? ( dev-libs/libusb:0.1 )
        run:
            pcmcia? ( sys-apps/setserial )
        suggestion:
            net-wireless/bluez-firmware[>=1.2] [[
                description = [ Firmware for BCM203x and STLC2300 Bluetooth chips ]
            ]]
            sys-auth/ConsoleKit2 [[
                description = [ Required by the provided D-Bus security policies ]
            ]]
    "
fi

# TODO: Tests?
# Many of these tests are meant to be installed and used by developers.
# Some might be suitable for src_test but there is no check/test target.
#    test:
#        gnome-bindings/pygobject
#        dev-python/dbus-python

DEFAULT_SRC_CONFIGURE_PARAMS=( --localstatedir=/var )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( cups )

if ever at_least 5.50 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-a2dp
        --enable-avrcp
        --enable-client
        --enable-datafiles
        --enable-debug
        --enable-experimental
        --enable-health
        --enable-hid
        --enable-hog
        --enable-library
        --enable-logger
        --enable-manpages
        --enable-monitor
        --enable-network
        --enable-optimization
        --enable-sap
        --enable-sixaxis
        --enable-testing
        --enable-threads
        --enable-udev
        --disable-android
        --disable-btpclient
        --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
        --with-systemduserunitdir=${SYSTEMDUSERUNITDIR}
        --with-udevdir=${UDEVDIR}
    )
    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
        'alsa midi'
        deprecated
        mesh
        nfc
        obex
        systemd
    )
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-bccmd
        --enable-pnat
        --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    )
    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
        alsa
        gstreamer
        health
        'legacy dund'
        'legacy hidd'
        'legacy pand'
        pcmcia
        usb
        'usb hid2hci'
        'usb dfutool'
    )
fi

bluez_src_install() {
    default

    insinto /etc/bluetooth
    if ever at_least 5.50 ; then
        doins profiles/input/input.conf
        doins profiles/network/network.conf

        doins src/main.conf
    else
        doins audio/audio.conf
        doins input/input.conf
        doins network/network.conf
        doins serial/serial.conf
    fi

    keepdir /var/lib/bluetooth

    install_openrc_files
}

bluez_pkg_postinst() {
    default
    nonfatal edo udevadm trigger --subsystem-match=bluetooth --action=change
}

bluez_pkg_postrm() {
    default
    nonfatal edo udevadm trigger --subsystem-match=bluetooth --action=change
}

