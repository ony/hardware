# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hughsie ] \
    meson [ meson_minimum_version=0.43.0 ] \
    python [ blacklist=2 multibuild=false has_lib=false ] \
    systemd-service \
    test-dbus-daemon
    udev-rules \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="A simple daemon to allow session software to update firmware"
HOMEPAGE+=" https://www.fwupd.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    efi [[ description = [ Support for flashing firmware via UEFI ] ]]
    dell [[
        description = [ Support for flashing firmware on DELL machines ]
        requires = [ efi ]
    ]]
    gobject-introspection
    gtk-doc
    systemd
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        sys-apps/help2man
        virtual/pkg-config
        efi? (
            fonts/dejavu
            gnome-bindings/pygobject:3[python_abis:*(-)?]
            dev-python/Pillow[python_abis:*(-)?]
            dev-python/pycairo[python_abis:*(-)?]
            media-libs/fontconfig
            media-libs/freetype:2
            sys-boot/efivar[>=33]
            sys-boot/gnu-efi
            x11-libs/cairo
            x11-libs/pango[gobject-introspection]
        )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        app-arch/gcab [[ note = [ colorhug and rpiupdate plugin ] ]]
        app-arch/libarchive
        app-crypt/gpgme
        core/json-glib[>=1.1.1]
        dev-db/sqlite:3
        dev-libs/appstream-glib[>=0.7.4][gobject-introspection?]
        dev-libs/glib:2[>=2.45.8]
        dev-libs/libgpg-error
        dev-libs/libgusb[>=0.3.0]
        dev-util/elfutils
        gnome-desktop/libgudev
        gnome-desktop/libsoup:2.4[>=2.51.92][gobject-introspection?]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-auth/polkit:1[>=0.113-r2] [[ note = [ itstools support ] ]]
        dell? ( sys-apps/libsmbios[>=2.4.0] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        systemd? ( sys-apps/systemd[>=231] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    -Ddaemon=true
    -Dconsolekit=false
    -Defi-cc=${CC}
    -Defi-includedir=/usr/$(exhost --target)/include/efi
    -Defi-ld=${LD}
    -Defi-libdir=/usr/$(exhost --target)/lib
    -Dgpg=true
    -Dlvfs=true
    -Dman=true
    -Dpkcs7=false
    -Dplugin_altos=true
    -Dplugin_amt=true
    -Dplugin_dummy=false
    -Dplugin_redfish=true
    -Dplugin_synaptics=true
    -Dplugin_thunderbolt=true
    -Dtests=false
    -Dsystemdunitdir=${SYSTEMDSYSTEMUNITDIR}
    -Dudevdir=${UDEVDIR}
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'efi plugin_uefi'
    'dell plugin_dell'
    'gobject-introspection introspection'
    'gtk-doc gtkdoc'
    'systemd'
)

src_prepare() {
    meson_src_prepare

    # https://github.com/hughsie/fwupd/issues/592
    edo sed \
        -e "s/'objcopy'/'${OBJCOPY}'/" \
        -e "s/'readelf'/'$(exhost --tool-prefix)readelf'/" \
        -i meson.build
}

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

src_install() {
    meson_src_install

    keepdir /var/lib/fwupd
}

